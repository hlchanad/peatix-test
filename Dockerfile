FROM node:14-alpine3.15 as builder

WORKDIR /app

COPY ./package* ./

RUN npm ci

COPY . .

EXPOSE 4000

CMD ["npm", "run", "serve"]
