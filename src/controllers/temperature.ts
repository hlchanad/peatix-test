import { validateAll } from 'indicative/validator';
import { lambda } from 'lambda-api-helper';

export const getTemperature = lambda.extend(
  {
    middlewares: [
      async (request, response, next) => {
        try {
          request.query = await validateAll(request.query, {
            degree: 'required|float',
            from: 'required|in:C,F',
          });

          await next();
        } catch (errors) {
          response.status(400).body({
            errors: errors.map(({ message }) => ({ message })),
          });
        }
      },
    ],
  },
  async (request) => {
    const { degree, from }: { degree: number; from: 'C' | 'F' } = request.query;

    let result: number;

    if (from === 'C') {
      result = (degree * 9) / 5 + 32;
    } else {
      result = ((degree - 32) * 5) / 9;
    }

    return { temperature: result };
  },
);
