import { mockEventCreator } from 'aws-lambda-test-utils';
import * as temperatureController from '../temperature';

describe('getTemperature', () => {
  let event, context, callback: jest.MockInstance<() => void, [any, any]>;

  beforeEach(async () => {
    event = mockEventCreator.createAPIGatewayEvent({});
    event.multiValueQueryStringParameters = {};

    context = {};

    callback = jest.fn();
  });

  describe('request validations', () => {
    it('requires `degree` in float', async () => {
      // required
      await temperatureController.getTemperature(event, context, callback);

      let call = callback.mock.calls[0];
      expect(call[0]).toBeNull();
      expect(call[1].statusCode).toBe(400);
      expect(call[1].body).toMatch(/required validation failed on degree/);

      // float
      event.queryStringParameters.degree = 'invalid';
      await temperatureController.getTemperature(event, context, callback);

      call = callback.mock.calls[1];
      expect(call[0]).toBeNull();
      expect(call[1].statusCode).toBe(400);
      expect(call[1].body).toMatch(/float validation failed on degree/);
    });

    it('requires `from` in `C` | `F`', async () => {
      // required
      await temperatureController.getTemperature(event, context, callback);

      let call = callback.mock.calls[0];
      expect(call[0]).toBeNull();
      expect(call[1].statusCode).toBe(400);
      expect(call[1].body).toMatch(/required validation failed on from/);

      // float
      event.queryStringParameters.from = 'invalid';
      await temperatureController.getTemperature(event, context, callback);

      call = callback.mock.calls[1];
      expect(call[0]).toBeNull();
      expect(call[1].statusCode).toBe(400);
      expect(call[1].body).toMatch(/in validation failed on from/);
    });
  });

  it('converts C to F', async () => {
    event.queryStringParameters = { from: 'C', degree: 40 };
    await temperatureController.getTemperature(event, context, callback);

    const call = callback.mock.calls[0];
    expect(call[0]).toBeNull();
    expect(call[1].statusCode).toBe(200);
    expect(JSON.parse(call[1].body).temperature).toBe(104);
  });

  it('converts F to C', async () => {
    event.queryStringParameters = { from: 'F', degree: 104 };
    await temperatureController.getTemperature(event, context, callback);

    const call = callback.mock.calls[0];
    expect(call[0]).toBeNull();
    expect(call[1].statusCode).toBe(200);
    expect(JSON.parse(call[1].body).temperature).toBe(40);
  });
});
