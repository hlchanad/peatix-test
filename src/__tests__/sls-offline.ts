import { spawn } from 'child_process';
import * as faker from 'faker';

const randomPort = (start = 30001, end = 40000): number =>
  faker.datatype.number(end - start) + start;

export const ServerlessOffline = {
  start: async ({ port = randomPort() }: { port?: number } = {}): Promise<{
    getBaseUrl: () => string;
    stop: () => void;
  }> => {
    const slsOfflineProcess = spawn(
      './node_modules/.bin/serverless',
      `offline start --httpPort ${port} --stage test`.split(' '),
      { shell: true, cwd: process.cwd() },
    );

    await new Promise<void>((resolve) => {
      slsOfflineProcess.stderr.on('data', (data) => {
        if (data.toString().includes('Server ready:')) {
          resolve();
        }
      });
    });

    return {
      getBaseUrl: () => `http://localhost:${port}/test`,
      stop: () => {
        slsOfflineProcess.stdin.write('q\n');
        slsOfflineProcess.kill();
      },
    };
  },
};
