import * as request from 'supertest';
import { ServerlessOffline } from './sls-offline';

jest.setTimeout(30 * 1000);

let server: Awaited<ReturnType<typeof ServerlessOffline.start>>;
let baseUrl: string;

beforeAll(async () => {
  server = await ServerlessOffline.start();
  baseUrl = server.getBaseUrl();
});

afterAll(async () => {
  server.stop();
});

it('gets temperature', async () => {
  let response = await request(baseUrl)
    .get('/temperature')
    .query({ degree: 40, from: 'C' });

  expect(response.body.temperature).toBe(104);

  response = await request(baseUrl)
    .get('/temperature')
    .query({ degree: 104, from: 'F' });

  expect(response.body.temperature).toBe(40);
});
