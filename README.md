# Peatix Test

It is a simple temperature service that converts temperature degree from Celsius or Fahrenheit.

---

## Requirements

1. [Docker](https://docs.docker.com/install/)

2. [Docker Compose](https://docs.docker.com/compose/install/)

## How to run it?

1. Clone the repository:

```shell
$ git clone git@bitbucket.org:hlchanad/peatix-test.git
```

2. Build the application

```shell
$ docker-compose build
```

3. (Optional) Run test suite

```shell
$ docker-compose run --rm server-test
```

4. Run the server

```shell
$ docker-compose up server
```

---

## Design of the service

### Understanding the characteristic of the service

This temperature is a simple service which only converts the degree
from Celsius or Fahrenheit. The complexity of the service would not
grow very largely. So the code structure could be kept as simple as
possible.

### Serverless framework

The framework provides an easy way to develop an AWS Lambda function
and deploy to AWS. And it fits the characteristic stated above.

### Coding

With the support of [lambda-api-helper](https://www.npmjs.com/package/lambda-api-helper)
(written by myself), request validation and main logic could be
separated and kept clean.

### Test

Unit test and integration test are provided as well.

---

## APIs

### Convert temperature from Celsius or Fahrenheit

#### Request

Path: `/temperature`

Query Parameters

- `degree` - float
- `from` - `'C'` or `'F'`

#### Response

```json
{
  "temperature": 104
}
```
